<?php

    $router = $di -> getRouter();

// Define your routes here

    $router -> add('/', 'App::index');

    $router -> add('/users/search', 'Users::search');

    $router -> add('/users/get/{id}', 'Users::get');

    $router -> add('/users/post', 'Users::post');

    $router -> add('/users/update/{id}', 'Users::update');

    $router -> add('/users/delete/{id}', 'Users::delete');


    $router -> handle($_SERVER[ 'REQUEST_URI' ]);
