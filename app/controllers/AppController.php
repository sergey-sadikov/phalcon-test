<?php
declare(strict_types=1);


class AppController extends ControllerBase
{

    public function indexAction()
    {
        $this->view->disable();
        echo file_get_contents(BASE_PATH.'/public/dist/index.html');
    }

}

