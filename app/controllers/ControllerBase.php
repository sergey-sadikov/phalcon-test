<?php
declare(strict_types=1);

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    public function getRequestBody()
    {
        return json_decode(file_get_contents('php://input'));
    }
}
