<?php
    declare( strict_types = 1 );

    use Phalcon\Mvc\Model\Query\Builder;

    class UsersController extends ControllerBase
    {
        public function searchAction()
        {
            $request = $this -> getRequestBody();
            $limit = $request -> pagination -> limit;

            $builder = new Builder([
                'models' => [ Users::class ],
            ]);

            if (strlen($request -> search) >= 3) {
                $builder -> andWhere("MATCH(name, surname, middleName)AGAINST('{$request->search} *') ");
            }

            $builderCount = $builder;

            $count = $builderCount -> getQuery() -> execute() -> count();

            $rows = $builder -> limit($limit) -> offset($limit * $request -> pagination -> page - $limit) -> getQuery() -> execute();


            echo json_encode([ 'rows' => $rows, 'count' => $count ]);
        }

        public function getAction($id)
        {
            return json_encode(Users ::find($id) -> toArray());
        }

        public function postAction()
        {
            $request = $this->getRequestBody();
            if($request->id) {
                $user = Users::findFirst(['id' => $request->id]);
            } else {
                $user = new Users();
            }
            $user->name = $request->name;
            $user->surname = $request->surname;
            $user->middleName = $request->middleName;
            if($user->save()) {
                echo 1;
            } else {
                $messages = $user->getMessages();

                foreach ($messages as $message) {
                    echo $message . PHP_EOL;
                }
            };
        }

        public function deleteAction($id)
        {
            $user = Users::findFirst(['id' => $id]);
            $user->delete();
        }
    }

