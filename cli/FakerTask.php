<?php

    declare( strict_types = 1 );

    namespace cli;

    use Phalcon\Cli\Task;

    class FakerTask extends Task
    {
        public function mainAction()
        {
            $faker = \Faker\Factory ::create('ru_RU');

            $this -> db -> begin();

            for ($i = 0; $i <= 1000; $i++) {
                $this -> modelsManager -> executeQuery(
                    "INSERT INTO Users (name, surname, middleName) VALUES (?0, ?1, ?2)",
                    [
                        $faker -> firstName,
                        $faker -> middleName,
                        $faker -> lastName,
                    ]
                );
            }

            $this -> db -> commit();

        }
    }