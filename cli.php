<?php

    declare(strict_types=1);

    use Phalcon\Cli\Console;
    use Phalcon\Cli\Dispatcher;
    use Phalcon\Di\FactoryDefault\Cli as CliDI;
    use Phalcon\Exception as PhalconException;


    define('BASE_PATH', dirname(__FILE__));
    define('APP_PATH', BASE_PATH . '/app');

    /**
     * The FactoryDefault Dependency Injector automatically registers the services that
     * provide a full stack framework. These default services can be overidden with custom ones.
     */
    $di = new CliDI();

    /**
     * Include Services
     */
    include APP_PATH . '/config/services.php';

    /**
     * Get config service for use in inline setup below
     */
    $config = $di->getConfig();

    include APP_PATH ."/config/loader.php";

    $dispatcher = new Dispatcher();

    $dispatcher->setDefaultNamespace('cli');
    $di->setShared('dispatcher', $dispatcher);

    $console = new Console($di);

    $arguments = [];
    foreach ($argv as $k => $arg) {
        if ($k === 1) {
            $arguments['task'] = $arg;
        } elseif ($k === 2) {
            $arguments['action'] = $arg;
        } elseif ($k >= 3) {
            $arguments['params'][] = $arg;
        }
    }

    try {
        $console->handle($arguments);
    } catch (PhalconException $e) {
        fwrite(STDERR, $e->getMessage() . PHP_EOL);
        exit(1);
    } catch (Throwable $throwable) {
        fwrite(STDERR, $throwable->getMessage() . PHP_EOL);
        exit(1);
    } catch (Exception $exception) {
        fwrite(STDERR, $exception->getMessage() . PHP_EOL);
        exit(1);
    }