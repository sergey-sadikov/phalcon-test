import http from "./http";

const api = {
    users: {
        search(params) {
            return http.post('users/search', params);
        },
        delete(id) {
            return http.post('users/delete/'+id);
        },
        post(params) {
            return http.post('users/post', params);
        },
        get(id) {
            return http.post('users/get/'+id);
        }
    }
};

export default api