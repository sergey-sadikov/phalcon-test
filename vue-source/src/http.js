import axios from 'axios';

const http = axios.create({});

http.interceptors.response.use(
    conf => {
        return conf;
    });

export default http